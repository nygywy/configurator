package com.chaogeng.configurator.model

data class Person(
    val name: String,
    val isProposer: Boolean,
    val preference: ArrayList<String>,
    var spouse: String?
)