package com.chaogeng.configurator.service

import com.chaogeng.configurator.model.Person

interface PairingService {
    fun process(persons: List<Person>?): List<Person>
}