package com.chaogeng.configurator.service.serviceImpl

import com.chaogeng.configurator.service.SelfDescribingNumberService

class SelfDescribingNumberServiceImpl : SelfDescribingNumberService {
    override fun findSelfDescribingNumber(upperbound: Int): List<Int> {
        val selfDescribingNumList = mutableListOf<Int>()
        for (i in 1..upperbound) {
            if (i.selfDescribing()) {
                selfDescribingNumList.add(i)
            }
        }
        return selfDescribingNumList
    }

    private fun Int.selfDescribing(): Boolean {
        val numberString = this.toString()
        if (numberString.length % 2 != 0) {
            return false
        }
        val pairs = numberString.chunked(2)
        val numberChar = numberString.toCharArray()
        for (pair in pairs) {
            val count = numberChar.count { it == pair[1] }
            if (pair[0].digitToInt() != count) {
                return false
            }
        }
        return true
    }
}