package com.chaogeng.configurator.service.serviceImpl

import com.chaogeng.configurator.exceptions.InvalidInputException
import com.chaogeng.configurator.model.Person
import com.chaogeng.configurator.service.PairingService
import java.util.*
class PairingServiceImpl : PairingService {
    override fun process(persons: List<Person>?): List<Person> {
        val checkedPersons = checkValidParameters(persons)
        return stableMarriage(checkedPersons)
    }

    private fun checkValidParameters(persons: List<Person>?): List<Person> {
        if (persons == null) {
            throw NullPointerException()
        }
        if (persons.count { it.isProposer } != persons.count { !it.isProposer }) {
            throw InvalidInputException("there must be an equal number of proposers and proposed")
        }
        if (persons.any { it.preference.size != persons.count { it.isProposer } }) {
            throw InvalidInputException("not all persons have valid preferences")
        }
        if (persons.any { person -> person.preference.groupingBy { it }.eachCount().any { (_, v) -> v >= 2 } }) {
            throw InvalidInputException("preference list must not have duplicates")
        }
        return persons
    }

    fun stableMarriage(people: List<Person>): List<Person> {
        val n = people.size / 2
        val men = people.filter { it.isProposer }.toMutableList()
        val women = people.filter { !it.isProposer }.toMutableList()
        val freeMen = LinkedList(men)
        val womenMap = mutableMapOf<String, Person>()
        women.forEach { womenMap[it.name] = it }

        while (freeMen.isNotEmpty()) {
            val man = freeMen.poll()
            for (i in 0 until n) {
                val womanName = man.preference[i]
                val woman = womenMap[womanName]!!
                if (woman.spouse == null) {
                    man.spouse = womanName
                    woman.spouse = man.name
                    break
                } else {
                    val currentMan = womenMap[woman.spouse]
                    if (woman.preference.indexOf(man.name) < woman.preference.indexOf(currentMan?.name)) {
                        man.spouse = womanName
                        woman.spouse = man.name
                        currentMan?.spouse = null
                        freeMen.offer(currentMan)
                        break
                    }
                }
            }
        }
        return people
    }

}