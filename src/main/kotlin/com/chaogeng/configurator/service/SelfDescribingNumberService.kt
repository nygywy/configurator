package com.chaogeng.configurator.service

interface SelfDescribingNumberService {
    fun findSelfDescribingNumber(upperbound: Int): List<Int>
}