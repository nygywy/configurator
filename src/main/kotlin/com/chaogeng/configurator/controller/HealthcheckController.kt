package com.chaogeng.configurator.controller

import io.swagger.v3.oas.annotations.Operation
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1")
class HealthcheckController {

    @GetMapping("/healthcheck")
    @Operation(summary = "Returns \"up\" if application is up")
    fun healthcheck(): String {
        return "up"
    }
}