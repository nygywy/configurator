package com.chaogeng.configurator.exceptions

class InvalidInputException(message: String) : Exception(message)
