package com.chaogeng.configurator.service.serviceImpl

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

class SelfDescribingNumberServiceImplTest {

    @Test
    fun findSelfDescribingNumberTo20() {
        val describingNumberServiceImpl = SelfDescribingNumberServiceImpl()
        val actualDescribingNumberList = describingNumberServiceImpl.findSelfDescribingNumber(20)
        val expectedDescribingNumberList = listOf(10,12,13,14,15,16,17,18,19)
        assertEquals(expectedDescribingNumberList, actualDescribingNumberList)
    }

    @Test
    fun findSelfDescribingNumberTo14233221() {
        val describingNumberServiceImpl = SelfDescribingNumberServiceImpl()
        val describingNumberList = describingNumberServiceImpl.findSelfDescribingNumber(14233221)
        assertTrue(describingNumberList.contains(14233221))
    }
}