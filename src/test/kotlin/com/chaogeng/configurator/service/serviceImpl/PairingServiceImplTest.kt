package com.chaogeng.configurator.service.serviceImpl

import com.chaogeng.configurator.exceptions.InvalidInputException
import com.chaogeng.configurator.model.Person
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class PairingServiceImplTest {
    @Test
    fun `throw a NPE if null object is passed`() {
        val pairingService = PairingServiceImpl()

        org.junit.jupiter.api.assertThrows<NullPointerException> {
            pairingService.process(null)
        }
    }

    @Test
    fun `empty list of person should return empty list`() {
        val pairingService = PairingServiceImpl()

        val configurationList = pairingService.process(arrayListOf())
        assertEquals(emptyList<Person>(), configurationList)
    }

    @Test
    fun `list of persons passed must be even or throw exception`() {
        val pairingService = PairingServiceImpl()

        val person1 = Person("random1", true, arrayListOf(), null)
        val person2 = Person("random2", true, arrayListOf(), null)
        val person3 = Person("random3", false, arrayListOf(), null)

        org.junit.jupiter.api.assertThrows<InvalidInputException> {
            pairingService.process(arrayListOf(person1, person2, person3))
        }
    }

    @Test
    fun `if list of preference not equal to total opposite gender, then throw exception`() {
        val pairingService = PairingServiceImpl()

        val person1 = Person("random1", true, arrayListOf("person2"), null)
        val person2 = Person("random2", false, arrayListOf(), null)

        org.junit.jupiter.api.assertThrows<InvalidInputException> {
            pairingService.process(arrayListOf(person1, person2))
        }
    }

    @Test
    fun `if list of preference has duplicates, then throw exception`() {
        val pairingService = PairingServiceImpl()

        val person1 = Person("random1", true, arrayListOf("random3", "random4"), null)
        val person2 = Person("random2", true, arrayListOf("random4", "random4"), null)
        val person3 = Person("random3", false, arrayListOf("random1", "random2"), null)
        val person4 = Person("random4", false, arrayListOf("random1", "random2"), null)

        org.junit.jupiter.api.assertThrows<InvalidInputException> {
            pairingService.process(arrayListOf(person1, person2, person3, person4))
        }
    }

    @Test
    fun `Given valid inputs, both persons should be paired`() {
        val pairingService = PairingServiceImpl()

        val person1 = Person("person1", true, arrayListOf("person2"), null)
        val person2 = Person("person2", false, arrayListOf("person1"), null)

        pairingService.process(arrayListOf(person1, person2))

        assertEquals(person2.name, person1.spouse)
        assertEquals(person1.name, person2.spouse)
    }

    @Test
    fun `Given valid inputs, all persons should be paired`() {
        val pairingService = PairingServiceImpl()

        val alex = Person("Alex", true, arrayListOf("Samantha", "Emma", "Lily"), null)
        val ben = Person("Ben", true, arrayListOf("Emma", "Lily", "Samantha"), null)
        val charlie = Person("Charlie", true, arrayListOf("Emma", "Lily", "Samantha"), null)
        val emma = Person("Emma", false, arrayListOf("Ben", "Charlie", "Alex"), null)
        val lily = Person("Lily", false, arrayListOf("Alex", "Ben", "Charlie"), null)
        val samantha = Person("Samantha", false, arrayListOf("Alex", "Ben", "Charlie"), null)


        pairingService.process(arrayListOf(alex, ben, charlie, emma, lily, samantha))

        assertEquals(alex.name, samantha.spouse)
        assertEquals(samantha.name, alex.spouse)
        assertEquals(emma.name, ben.spouse)
        assertEquals(ben.name, emma.spouse)
        assertEquals(charlie.name, lily.spouse)
        assertEquals(lily.name, charlie.spouse)
    }
}